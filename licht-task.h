
#define FI_TIGGER_PIN 2
#define FI_READ_PIN 3

class Lichttask {

    uint8_t connections[4] = {0};
    boolean fiState = false;
    uint16_t colorVals[5] = {0, 523, 197, 688, 346,};

  public:
    Lichttask() {}
    void begin() {
      pinMode(FI_TIGGER_PIN, OUTPUT);
      pinMode(FI_READ_PIN, INPUT);

      //initialize default state
      digitalWrite(FI_TIGGER_PIN, LOW);
    }

    void triggerFI(void) {
      digitalWrite(FI_TIGGER_PIN, HIGH);
      delay(10);
      digitalWrite(FI_TIGGER_PIN, LOW);
    }

    boolean isFIOn() {
      return digitalRead(FI_READ_PIN);
    }

    uint8_t getColor(int input) {
      uint16_t v = analogRead(input);

      //find nearest value in array
      uint16_t m = UINT16_MAX;
      uint8_t m_i = 0;
      for (uint8_t i = 0; i < 5; i++) {
        int16_t diff = v - colorVals[i];
        if ( abs(diff) < m) {
          m = abs(diff);
          m_i = i;
        }
      }
      return m_i;
    }

    boolean changed() {
      boolean changed = false;
      if (fiState != isFIOn()) {
        //debounce
        delay(50);
        fiState = isFIOn();
        changed = true;
      }

      for (int i = 0; i < 4; i++) {
        if (connections[i] != getColor(i)) {
          //debounce
          delay(50);
          connections[i] = getColor(i);
          changed = true;
        }
      }
      return changed;
    }

};
