#include <stdint.h>

#include <SPI.h>
#include <Ethernet.h>
#include <ArduinoJson.h>
#include <utility/w5100.h>

#include "licht-task.h"

#define JSON_BUF_SIZE 100

Lichttask l;


// Enter a MAC address and IP address for your controller below.
byte mac[] = {
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED
};
IPAddress ip(192, 168, 178, 200);

// IP Address of the remote server
IPAddress remote(192, 168, 178, 51);

// Ethernet server instance with the port you want to use
EthernetServer server(8080);

void serve() {
  EthernetClient client = server.available();
  if (client) {
    char buf[JSON_BUF_SIZE] = {0};
    boolean error = false;
    client.setTimeout(1000);
    Serial.print("IN: ");

    while (client.connected()) {
      if (!client.find("ength: ")) {
        error = true;
        Serial.println("Content-Length not found");
        break;
      }

      String s = client.readStringUntil('\n');
      size_t len = s.toInt();

      if (len < 1) {
        error = true;
        Serial.println("Content-Length < 1");
        break;
      }

      int c;
      boolean emptyLine = false;
      while ((c = client.read()) != -1) {
        if (c == '\n' && emptyLine)
          break;
        if (c == '\n') {
          emptyLine = true;
          continue;
        }
        if (c != '\r')
          emptyLine = false;
      }

      if (len > sizeof(buf)) len = sizeof(buf) - 1;

      client.readBytes(buf, len);
      Serial.print(buf);
      client.print("HTTP/1.1 200 OK\n\n");
      break;

    }
    // give the web browser time to receive the data
    delay(1);
    // close the connection:
    client.stop();
    Serial.println(" OK");

    StaticJsonBuffer<JSON_BUF_SIZE> jsonInBuffer;
    JsonObject& jsData = jsonInBuffer.parseObject(buf);

    if (jsData.containsKey("FI")) {
      l.triggerFI();
    }
  }
}


void request(JsonObject& data) {
  Serial.print("OUT: ");


  // Ethernet Client instance
  EthernetClient client;

  // if you get a connection, report back via serial:
  if (client.connect(remote, 8081)) { // plaiframe: 8080
    client.print("GET /Tutorial/devices/http/LichtTask"); //plaiframe: lichttask
    client.print("?data=");
    data.printTo(client);
    data.printTo(Serial);
    client.println();
    client.println();

    while (client.connected()) {
      if (client.available()) {
        char c = client.read();
        Serial.print(c);
      }
    }
    client.stop();
    Serial.println(" OK");
  } else {
    // if you didn't get a connection to the server:
    Serial.println("ERR");
  }
}




void setup() {
  Serial.begin(115200);
  l.begin();

  // Init Ethernet
  Ethernet.begin(mac, ip);
  // give the Ethernet shield a second to initialize:
  delay(1000);

 // W5100.setRetransmissionTime(2000);   //2000 is the default value, equivalent to 200ms
 // W5100.setRetransmissionCount(2);     //8 is the default value

  // Start server
  server.begin();
  Serial.print("server runs at ");
  Serial.println(Ethernet.localIP());


}


char * val2color[] ={"none","orange","blue","red","yellow"};

void loop() {
  serve();
  if (l.changed()) {
    StaticJsonBuffer<JSON_BUF_SIZE> jsonOutBuffer;
    JsonObject& sendData = jsonOutBuffer.createObject();
    sendData["orange"] =  val2color[l.getColor(0)];
    sendData["blue"] =  val2color[l.getColor(1)];
    sendData["red"] =  val2color[l.getColor(2)];
    sendData["yellow"] =  val2color[l.getColor(3)];
    sendData["FI"] = l.isFIOn();
    request(sendData);
  }
}
